#!/bin/bash
# Test a droplist for obvious mistakes
#set -xv

DROPLIST=$1

for mask in $(seq 0 21)
do
  if [[ $(grep -E -o "/${mask}[^0-9]|/${mask}$" "$DROPLIST" -c) -gt 0 ]]
  then 
   echo mask "${mask}" is too big a subnet
   grep -E "/${mask}[^0-9]|/${mask}$" "$DROPLIST"
   exit 1
  fi
done

